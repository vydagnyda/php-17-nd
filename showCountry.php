<?php
require_once 'Country.php';
require_once 'City.php';

$id = $_GET['id'];
$country = Country::findOne($id);
$citiesByCode = City::findCities($id);

?>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
<a href="countries.php">Atgal</a>
<br>
<h1 style="text-align:center;"><?php echo $country['name']; ?></h1>
<br>
<div class="container"> 
<table class="table table-striped">
  <thead>
    <tr> 
      <th scope="col">Šalies kodas</th>
      <th scope="col">Šalies pavadinimas</th>
      <th scope="col">Kraštovaizdis</th>
    </tr>
  </thead>
  <tbody>
  <tr>   
      <td><?php echo $country['code']; ?></td>
      <td><?php echo $country['name']; ?></td>
      <td><?php echo $country['surface']; ?></td>  
    </tr>
    </tbody>
</table>
<br>

<h4 style="text-align:center;">Šalies miestai</h4>
<br>
<table class="table table-striped">
  <thead>
    <tr> 
      <th scope="col">#</th>
      <th scope="col">Šalies miestas</th>    
      <th scope="col">Populiacija</th>   
      <th scope="col">Veiksmai</th>  
    </tr>
  </thead>
  <tbody>
     <?php 
    $i = 1;
    foreach ($citiesByCode as $cityItem) { ?>
    <tr>   
      <th scope="row"><?php echo $i++; ?></th>
      <td><?php echo $cityItem['name']; ?></td> 
      <td><?php echo $cityItem['population']; ?></td>      
      <td>
      <form action="deleteCity.php" method="POST">
        <input type="submit" value="Ištrinti">
        <input type="hidden" value="<?php echo $cityItem['id']; ?>" name="id">
        <button><a href="editCity.php?id=<?php echo $cityItem['id']; ?> ">Atnaujinti</a></button>         
      </form>
     </td> 
    </tr>
    <?php }?>
 </tbody>
</table>

<button><a href="createCity.php?country=<?php echo $cityItem['country_code']; ?> ">Pridėti naują miestą</a></button>
</div>
</body>

</html>