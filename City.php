<?php
require_once 'DBConnectionTrait.php';

class City
{
    use DBConnectionTrait;
    //protected $id; //int
    protected $name;
    protected $countryCode;
    protected $district;
    protected $population; // int

    public function __construct( string $name, string $countryCode, string $district, int $population)
    {       
        $this->name = $name;
        $this->countryCode = $countryCode;
        $this->district = $district;
        $this->population = $population;
        self::loadDatabase();
    }

    public static function findAllCities(): array
    {
        self::loadDatabase();
        $query = self::$pdo->prepare('SELECT id, name, country_code, district, population FROM city');
        $query->execute();

        return $query->fetchAll();
    }

    public static function findCities(string $countryCode): array
    {
        self::loadDatabase();
        $query = self::$pdo->prepare('SELECT id, name, country_code, district, population FROM city WHERE country_code = :country_code GROUP BY name');
        $query->execute(['country_code' => $countryCode]);

        return $query->fetchAll();
    }


    public function saveCity(): bool
    {
        self::loadDatabase();
        $sql = 'INSERT INTO city(name, district, population, country_code) VALUES (:name, :district, :population, :country_code)';
        $query = self::$pdo->prepare($sql);    
      
        return $query->execute([
            'name'=> $this->name,
            'district'=> $this->district,
            'population'=> $this->population,
            'country_code' => $this->countryCode]);    
    } 

    public static function deleteCity(int $id) : bool
    {
        self::loadDatabase();      
        $query = self::$pdo->prepare('DELETE FROM city WHERE id=:id');

        return $query->execute(['id' => $id]);
    }

    public static function updateCity(int $id, string $name, string $district, int $population, string $country_code): bool
    {
        self::loadDatabase(); 
        $sql = 'UPDATE city
        SET name=:name,
            district=:district,
            population=:population,
            country_code=:country_code       
        WHERE id=:id';

        $query = self::$pdo->prepare($sql);

        return $query->execute([
        'id' => $id,
        'name' => $name,
        'district' => $district,
        'population' => $population,
        'country_code' => $country_code,     
        ]);
    }    


        /**
         * Get the value of name
         */ 
        public function getName()
        {
            return $this->name;
        }

        public static function getCity(int $id)
        {
            self::loadDatabase(); 
            $query = self::$pdo->prepare('SELECT id, name, district, population, country_code FROM city WHERE id=:id');
            $query->execute(['id' => $id]);
            return $query->fetch(); 
        }
}
