<?php
require_once 'Country.php';
require_once 'City.php';

$countries = Country::findAll();
//var_dump($countries);

?>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
<h1 style="text-align:center;">Šalys</h1>
<br>
<div class="container"> 
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Šalies kodas</th>
      <th scope="col">Šalies pavadinimas</th>
      <th scope="col">Kraštovaizdis</th>
    </tr>
  </thead>
  <tbody>
  <?php 
    $i = 1;
    foreach ($countries as $countryItem) { ?>
    <tr>   
      <th scope="row"><?php echo $i++; ?></th>
      <td><?php echo $countryItem['code']; ?></td>
      <td><a href="showCountry.php?id=<?php echo $countryItem['code']; ?>"><?php echo $countryItem['name']; ?></a></td>
      <td><?php echo $countryItem['surface']; ?></td>  
    </tr>

    <?php }?>
  </tbody>
</table>

</div>
</body>

</html>