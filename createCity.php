<?php
require_once 'City.php';
$country_code = $_GET['country'];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  
    $City = new City($_POST['name'], $country_code, $_POST['district'], (int) $_POST['population']);
    $City->saveCity();

    header('Location:countries.php');
    exit();
}    
?>

<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <h1>Naujas miestas</h1>
        <br>
        <form method="POST">
            <div class="labels">
                Pavadinimas: <input type="text" name="name">
            </div>
            <br>
            <div class="labels">
                Apskritis: <input type="text" name="district">
            </div>
            <br>
            <div class="labels">
                Populiacija: <input type="text" name="population">
            </div>
              
            <br>
            <!-- <div class="labels">
                Šalies kodas: <input type="text" name="country_code">
            </div> -->
            <br>
            <input name="id" type="hidden" />
            <input type="submit" value="Patvirtinti">
            
        </form>
    </div>
</body>
</html>